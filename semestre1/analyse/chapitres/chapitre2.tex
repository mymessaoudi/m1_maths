\chapter{Théorèmes fondamentaux de l'analyse fonctionnelle}
\section{Espaces métriques complets et théorème de Baire}
\subsection{Rappels sur la complétude}
\subsubsection{Définitions}
\begin{Def}
Soit $(E,\d)$ un espace métrique, on dit qu'une suite $(x_n)_n$ est de Cauchy si:
\[\forall\varepsilon>0,\exists N\in\N,\ \forall p>q\geq N,\ \d(x_p,x_q)<\varepsilon\]
ou encore $\d(x_p,x_q)\xrightarrow[n\tto+\infty]{}0$.\medbreak
Un espace métrique est complet si toute suite de Cauchy est convergente. On appelle espace de Banach tout evn complet. On appelle \textbf{algèbre normé} tout evn $(E,\norm{\cdot})$ muni d'une loi de multiplication vérifiant que : $\forall x,y\in E,\ \norm{x\cdot y}\leq\norm{x}\norm{y}$. Elle est dite de Banach si l'evn sous-jacent est complet.
\end{Def}
\subsubsection{Première proposition}
\begin{enumerate}[label=(\roman*),leftmargin=*]
\item Dans un espace métrique toute suite convergente est de Cauchy
\item Toute suite de Cauchy est bornée, a au plus une valeur d'adhérence et si elle en a une, elle converge.
\item Si un espace vectoriel $E$ est muni de 2 normes $N,N'$ alors $(E,N)$ complet $\iff (E,N')$ complet et une suite est de Cauchy dans $(E,N)$ \ssi elle est de Cauchy dans $(E,N')$.\\
Attention cependant, la complétude n'est pas une notion topologique, mais métrique, elle n'est donc pas conserver par homéomorphisme.
\item Soit $(E,\d)$ un espace métrique et $A\subset E$. Alors si $A$ est complet, il est fermé. Si $E$ est complet et $A$ est fermé, $A$ est complet. Si $A$ compact, il est complet.
\item Un espace est de Banach \ssi toute suite absolument convergente converge.
\end{enumerate}
\subsubsection{Exemples importants d'espace complets}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item $(\R,|\cdot|)$
\item $\M_n(\K)$
\item $\ell^\infty(I,F)$ l'espace des fonctions bornées de $I$ dans $F$ espace de Banach pour la norme $\norm{\cdot}_\infty$.
\item Si $(K,\d)$ compact, $C^0(K,F)$ complet par $\norm{\cdot}_\infty$ car fermé dans $(\ell^\infty(K,F),\norm{\cdot}_\infty)$.
\item $(L^p(I),\norm{\cdot}_p)$ pour $1\leq p\leq +\infty$.
\item $(\mathcal{S}(\R),\d)$ est complet
\item $\L_c(E,F)$ où $E,F$ deux evn et $F$ de Banach.
\end{itemize}
\subsubsection{Trois théorèmes utilisant la complétude}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item Théorème du point fixe de Picard
\item critère de Cauchy + théorème du prolongement
\item théorème des fermés emboités
\end{itemize}
\begin{thrm}{ (du point fixe)}
Soit $(E,\d)$ un espace métrique complet, et une application $f:E\to E$ telle que:
\[\exists k\in]0,1[,\ \forall(x,y)\in E^2,\quad	 \d(f(x),f(y))\leq k\d(x,y)\]
on dit alors que $f$ est $k$-contractante. Alors $f$ admet un unique point fixe i.e. il existe un unique $x\in E$ tel que $f(x)=x$.
\end{thrm}
\begin{pr}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item \textbf{Existence.} Fixons $x_0\in E$. On définit la suite $(x_n)$ par $x_{n+1}=f(x_{n})$.\\
Puisque $\d(x_{n+1},x_n)=\d(f(x_n),f(x_{n-1}))\leq k\d(x_n,x_{n-1})$, une récurrence immédiate donne $\d(x_{n+1},x_n)\leq k^n \d(x_1,x_0)$ pour tout entier $n\in\N$. Donc pour $p<q$, on a par inégalité triangulaire:
\[\d(x_p,x_q)\leq \d(x_p,x_{p+1})+\d(x_{p+1},x_{p+2})+\cdots+\d(x_{q-1},x_q)\leq (k^p+\cdots+k^{q-1})\d(x_0,x_1)\leq\frac{k^p}{1-k}\d(x_1,x_0)\widehat{=}Ck^p\]
Donc $(x_n)$ est de Cauchy $(Ck^p\xrightarrow[p\to\infty]{}0)$, $E$ étant complet, elle converge, notons $x$ sa limite. $f$ étant continue, car $k$-lipschitzienne on a :
\[f(x)=\lim_{n\infty}f(x_n)=\lim_{n\infty}x_{n+1}=x\]
Le point $x$ est donc un point fixe de $f$.
\item \textbf{Unicité.} Soit $(x,y)\in E^2,\ f(x)=x$ et $f(y)=y$, donc:
\[\d(x,y)=\d(f(x),f(y))\leq k\d(x,y)<\d(x,y)\]
qui n'est possible que si $\d(x,y)=0\iff x=y$ d'où l'unicité.
\end{itemize}
\end{pr}

\begin{thrm}{ des fermés emboités}
Soit $(E,\d)$ espace complet et $(F_n)_n$ suite décroissante de fermés non vides de $E$ telle que $\lim_{n\tto+\infty}\delta(F_n)=0$. Alors il existe $x\in E$ telle que $\bigcap_{n\geq 0}F_n=\{x\}$.
\end{thrm}
\begin{pr}\ \\
Soit $(x_n)_n$ suite de $E$ telle que $\forall n\in\N,\ x_n\in F_n$ alors pour tout $p>q\in\N,\ x_p\in F_q$ car $(F_n)$ décroissante, donc $\d(x_p,x_q)\leq \delta(F_q)\xrightarrow[p,q\tto+\infty]{}0$ donc $(x_n)_n$ est de Cauchy dans $E$ complet, elle converge donc vers $x\in E$.\\
Pour tout $p\in N,\ F_p$ est fermé et pour tout $n\geq p,\ x_n\in F_p$ donc la limite de $(x_n)_n$ appartient à $F_p$; cela pour tout $p\in \N$ donc finalement $x\in\cap_{n\geq 0}F_n$.\\
Et finalement, si $x,y\in\cap_{n\geq 0}F_n$ on a $\d(x,y)\leq\delta(F_n)\xrightarrow[n\infty]{}0$ donc $\d(x,y)=0$ d'où finalement $\cap_{n\geq 0}F_n=\{x\}$.
\end{pr}

\subsection{Le théorème de Baire}
\begin{thrm}{ de Baire} 
Dans $(E,\d)$ espace métrique complet:
\begin{itemize}[label=\textbullet,leftmargin=*]
\item toute intersection dénombrable d'ouverts dense est dense
\item de façon équivalente, toute réunion dénombrable de fermé d'intérieur vide est d'intérieur vide
\end{itemize}
\end{thrm}
\begin{pr}
Soit $(O_n)_n$ suite d'ouvert dense de $E$, il suffit de montrer que tout ouvert de $U$ de $E$ intersecte $O=\bigcup_{n\geq 0} O_n$. Pour cela on va construire par récurrence une suite $(B_n)$ de boules fermées de $E$ vérifiant:
\begin{enumerate}[label=(\roman*),leftmargin=*]
\item $\forall n\in\N,\ B_n$ soit fermé et de rayon inférieur à $\frac{1}{2^n}$.
\item $B_0\subset O_0\cap U$ et pour $n\geq 0,\ B_{n+1}\subset O_{n+1}\cap \overset{\circ}{B_{n}}$
\end{enumerate}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item $O_0$ étant un ouvert dense dans $E$ il intersecte $U$ et on a l'existence d'une boule $\B(x_0,r)\subset O_o\cap U$ (car ouvert comme intersection d'ouvert). On pose alors $B_0$ comme la boule fermée centrée en $x_0$ et de rayon $\min(r/2,1)$, on aura alors bien $B_0\subset O_0\cap U$.
\item Supposons $(B_0,\dots,B_n)$ construites et vérifiant (i) et (ii). On a $O_{n+1}$ qui est un ouvert dense dans $E$, il intersecte donc l'ouvert $\overset{\circ}{B_n}$  et on a l'existence d'une boule ouverte $B(x,r)\subset O_{n+1}\cap\overset{\circ}{B_n}$ on pose alors $B_{n+1}$ la boule fermée centré en $x$ et de rayon $\min(r/2,2^{n+1})$. Alors $B_{n+1}\subset O_{n+1}\cap\overset{\circ}{B_n}$ et donc vérifie bien (i) et (ii).\\
\end{itemize}
Donc on a $(B_n)_n$ une suite décroissante de fermés non vide donc le diamètre tend vers 0. Le théorème des fermés emboités nous assure que $\cap_{n\geq 0}B_n=\{x\}$. De plus on a $x\in B_0\subset U$ et pour $n\geq 0,\ x\in B_n\subset O_n$ donc finalement $x\in O\cap U$ d'où $O$ est dense dans $E$.\medbreak
De là, puisque qu'une partie est dense ssi son complémentaire est d'intérieur vide: $\bar{A}=E\backslash(\overbrace{E\backslash A}^\circ$)\\
On aura donc si: $(F_n)_n$ suite de fermés d'intérieur vide, on a $(E\backslash F_n)_n$ suite d'ouverts dense de $E$, donc $\cap_{n\geq0}E\backslash F_n=E\backslash \cup_{n\geq 0}F_n$ est dense dans $E$ d'où $\cup_{n\geq 0}F_n$ est d'intérieur vide.
\end{pr}
\begin{Corollaire}
Dans un espace métrique complet, si $(F_n)_n$ suite de fermées de $E$ tel que $E=\bigcup_{n\in\N} F_n$, alors $\Omega=\bigcup_{n\geq 0}\overset{\circ}{F_n}$ est un ouvert dense de $E$.
\end{Corollaire}
\begin{pr}
Il s'agit de montrer que le fermé $G=E\backslash \Omega$ soit d'intérieur vide. Or pour tout $n\in\N,\ \overbrace{G\cap F_n}^{\circ}\subset \overset{\circ}{G}\cap\overset{\circ}{F_n}\subset G\cap\overset{\circ}{F_n}=\emptyset$. Donc $(G\cap F_n)_n$ est une suite de fermée d'intérieur vide de $E$, par théorème de Baire l'ensemble:
\[\bigcup_{n\geq 0}G\cap F_n=G\cap\bigcup_{n\geq 0}F_n=G\cap E=G\]
est d'intérieur vide. D'où $\Omega$ est dense dans $E$.
\end{pr}
\begin{Prop}
Soit $(E,\d)$ un espace métrique complet, $(f_n)_n$ une suite de fonction continues de $E$ dans $\R$ qui converge simplement vers $f$ sur $E$. Alors: l'ensemble des points de continuité de $f$ est dense dans $E$.
\end{Prop}
\begin{pr}
Notons $Cont(f)$ l'ensemble des points de continuité de $f$. On pose pour $\varepsilon>0$:
\[O_\varepsilon=\{x\in E\mid \exists V\in\mathcal{V}(x),\ \forall y,z\in V,\ |f(y)-f(z)|<\varepsilon\}\]
Alors tous les $O_\varepsilon$ sont des ouverts de $E$ (car voisinage de chacun de ses points) et on a :
\[Cont(f)=\bigcap_{n\geq 1}O_{\frac{1}{n}}\]
\begin{itemize}[label=\textbullet,leftmargin=*]
\item En effet si $x\in Cont(f)$ pour tout $n\geq 1,\ \exists\alpha>0,\ \forall y\in E,\ \d(x,y)<\alpha\Longrightarrow |f(x)-f(y)|<\tfrac{1}{2n}$ donc pour tout $y,z\in\B(x,\alpha),\ |f(y)-f(z)|\leq |f(y)-f(x)|+|f(x)-f(y)|<1/n$ donc $x\in O_\frac{1}{n}$. D'où $Cont(f)\subset O_\frac{1}{n}$.
\item Si $x\in O_\frac{1}{n}$ pour tout $n\geq 1,\ \exists \alpha>0,\ \forall y,z\in \B(x,\alpha)\Longrightarrow |f(y)-f(z)|<1/n$\\
Donc en particulier, $\forall y\in E,\ \d(x,y)<\alpha\Longrightarrow |f(x)-f(y)|<1/n$. Donc pour tout $\varepsilon >0$, si $\varepsilon\geq 1$, alors $|f(x)-f(y)|<1/n\leq\varepsilon$ et si $\varepsilon<1$, on pose $n_0=\lfloor 1/\varepsilon\rfloor$ donc $|f(x)-f(y)|<1/n_0\leq \varepsilon$. Donc finalement, pour tout $\varepsilon>0$:
\[\exists\alpha>0,\ \forall y\in E,\ \d(x,y)<\alpha\Longrightarrow|f(x)-f(y)|<\varepsilon\]
donc $x\in Cont(f)$ d'où l'égalité voulu.
\end{itemize}
Il suffit donc de montrer que les $O_\varepsilon$ sont denses dans $E$, le théorème de Baire nous assurera la densité de $Cont(f)$. On pose pour $n\in\N$:
\[F_n=\left\{x\in E\ \mid \forall p,q\geq n,\ |f_p(x)-f_q(x)|\leq\frac{\varepsilon}{3}\right\}\]
La continuité des $(f_n)_n$ nous assure, par caractérisation séquentielle, que $F_n$ est fermé. De plus la suite $(f_n(x))_n$ converge car $(f_n)_n$ converge, elle est donc de Cauchy, d'où :
\[E=\bigcup_{n\geq 0}F_n\]
Le lemme de Baire nous assure que $\Omega=\bigcup_{n\geq 0}\overset{\circ}{F_n}$ est dense dans $E$. Montrons que $\Omega\subset O_\varepsilon$ pour tout $\varepsilon>0$. Soit $x\in\Omega$, il existe $n_0\in\N$ et $V_1$ voisinage de $x$ tel que $V_1\subset F_{n_0}$, donc pour tout $(y,z)\in V_1^2$ et $p\geq n_0$:
\[|f_p(y)-f_p(z)|\leq |f_p(y)-f_{n_0}(y)|+|f_{n_0}(y)-f_{n_0}(z)|+|f_{n_0}(z)-f_{p}(z)|\leq\frac{2\varepsilon}{3}+|f_{n_0}(y)-f_{n_0}(z)|\]
et en faisant tendre $p\tto+\infty$ on a :$|f(y)-f(z)|\leq 2\varepsilon/3+|f_{n_0}(y)-f_{n_0}(z)|$. De plus $f_{n_0}$ est continue, il existe donc un voisinage $V_2$  de $x$ tel que pour tout $(y,z)\in V_2^2,\ |f_{n_0}(y)-f_{n_0}(z)|<\varepsilon/3$. Donc en prenant $V=V_1\cap V_2$ qui est un voisinage de $x$ on a :\(\forall (y,z)\in V^2,\ |f(z)-f(y)|<\varepsilon\) donc $x\in O_\varepsilon$.
\end{pr}
\subsection{Le théorème de Banach-Steinhaus}
\begin{thrm}{ de Banach-Steinhaus}
Soit $E$ un espace de Banach, $F$ un evn et $(\varphi_\alpha)_{\alpha\in A}$ une famille d'application linéaire continue de $E$ dans $F$ vérifiant:
\[\forall x\in E,\ \sup_{\alpha\in A}\norm{\varphi_\alpha(x)}_F<+\infty\]
une famille "ponctuellement bornée" alors:
\[\sup_{\alpha\in A}\norms{\varphi_\alpha}<+\infty\]
la famille est "uniformément bornée":
\[\exists M>0,\ \forall\alpha\in A,\ \forall x\in E,\ \norm{\varphi_\alpha(x)}_F\leq M\norm{x}_E\]
\end{thrm}
\begin{pr}

\end{pr}
\begin{Corollaire}
Soit $E$ un espace de Banach, $F$ un evn et $(\varphi_n)_n$ une suite d'éléments de $\L_c(E,F)$ avec $\varphi_n\xrightarrow[E]{CVS}\varphi$. Alors la limite simple $\varphi$ est une application linéaire continue et $\norms{\varphi}\leq\liminf\norms{\varphi_n}$.
\end{Corollaire}
\begin{pr}
Une limite simple d'application linéaire est linéaire. Reste la continuité de $\varphi$. Si $x\in E$ la suite $(\varphi_n(x))_n$ converge dans $F$ vers $\varphi(x)$ donc elle est bornée : $\sup_{n\geq 0}\norm{\varphi_n(x)}_F=M<+\infty$. Donc d'après le théorème de Banach Steinhaus, $\sup_{n\geq 0}\norms{\varphi_n}<+\infty$ soit $\exists M>0,\ \forall x\in E,\ \forall n\in\N,\ \norm{\varphi_n(x)}_F\leq M\norm{x}_E$. En faisant tendre $n\tto+\infty$:
\[\norm{\varphi(x)}_F\leq M\norm{x}_E\]
ainsi $\varphi$ est linéaire continue et $\norms{\varphi}\leq M\Longrightarrow \norms{\varphi}\leq\liminf\norms{\varphi}$.
\end{pr}
\subsubsection{Application aux séries de Fourier}
Si $f:\R\to\C$ une fonction $2\pi-$périodique intégrable sur $[-\pi,\pi]$, on définit ses coefficient de Fourier:
\[\forall k\in\Z,\ c_k(f)=\frac{1}{2\pi}\int_{-\pi}^{\pi}f(t)\e^{-ikt}\dt\]
et les sommes partielles de Fourier:
\[\forall n\geq 0,\ S_n(f)(t)=\sum_{k=-n}^nc_k(f)\e^{ikt}\]
\begin{Prop}
Soit $E=C_{2\pi}(\R,\C)$ muni de $\norm{\cdot}_\infty$. Alors il existe $f\in E,\ S_n(f)(0)$ diverge et $\{f\in E\ \mid\ S_n(f)\text{ diverge}\}$ est dense dans $E$.
\end{Prop}
\begin{pr}

\end{pr}
\subsection{Théorème de l'application ouverte}
\begin{thrm}{ de l'application ouverte}
Soit $E,F$ deux espaces de Banach et $\varphi:E\to F$ linéaire continue surjective. Alors:
\begin{itemize}[label=\textbullet,leftmargin=*]
\item $\exists c>0,\ \varphi(\B_E(0,1))\supset \B_F(0,c)$
\item En particulier, $\varphi$ est ouverte.
\end{itemize}
\end{thrm}
\begin{pr}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item Le 2ème point découle du premier. En effet si $\varphi(\B_E(0,1))\supset\B_F(0,c)$ et si $U$ ouvert de $E$ et $y_0\in\varphi(U)$ il existe $x_0\in U,\ y_0=\varphi(x_0)$. Puisque $U$ ouvert de $x_0$:
\[\exists r>0,\ \B_E(x_0,r)\subset U\]
De plus, puisque $E,F$ des evn, on a : $\B_E(x_0,r)=x_0+r\B_E(0,1)$. Alors on aura:
\[y_0+\underbrace{\varphi(\B_E(0,1)r)}_{=r\varphi(\B_E(0,1)\supset r\B_F(0,c)}\subset \varphi(U)\]
Donc, finalement, $\B_F(y_0,rc)\subset \varphi(U)$ et donc $\varphi(U)$ est voisinage de $y_0$, vrai pour tout $y_0\in\varphi(U)$ c'est donc un ouvert de $F$.
\item Pour le premier point, on va procéder en trois étapes:
\begin{itemize}[label=\textbullet,leftmargin=*]
\item 
\end{itemize}
\end{itemize}
\end{pr}
\begin{thrm}{ d'isomorphisme de Banach}
Soit $E,F$ deux espace de Banach et $\varphi\in\L_c(E,F)$. Si $\varphi$ est bijective, alors $\varphi^{-1}\in \L_c(F,E)$ est un homéomorphisme.
\end{thrm}
\begin{pr}
Il s'agit de montrer que $\varphi^{-1}$ est continue, donc que l'image réciproque par $\varphi^{-1}$ de tout ouvert de $E$ est un ouvert de $F$. Soit $U$ ouvert de $E$, d'après le théorème de l'application ouverte, $(\varphi^{-1})^{-1}(U)=\varphi(U)$ est un ouvert de $F$, car $\varphi$ bijective, donc $\varphi^{-1}$ est continue.
\end{pr}
\section{Théorème de Hahn-Banach}
\subsection{Forme analytique}
\begin{thrm}{}
Soit $E$ un $\R$-ev normé, $F$ un sev de $E$ et $f\in\L_c(F,\R)$. Alors: $f$ peut-être prolongée en une forme linéaire continue $\tilde{f}$ sur $E$ de sorte que $\norms{f}=\norms{\tilde{f}}$.
\end{thrm}
\begin{pr}

\end{pr}
\begin{Corollaire}
Soit $F$ un sev d'un $\R$-evn sur $E,\ x_0\in E$. Alors \lasse:
\begin{enumerate}[label=(\roman*),leftmargin=*]
\item $x_0\notin \bar{F}$
\item il existe une forme linéaire continue $f$ sur $E$ telle que : $\forall x\in F,\ f(x)=0\ f(x_0)\neq 0$
\item il existe $H$ hyperplan fermé tel que $F\subset H$ et $x_0\notin H$. On dit que $H$ sépare strictement $F$ et $x_0$.
\end{enumerate}
\end{Corollaire}
\begin{pr}
\begin{itemize}[label=\textbullet,leftmargin=*]
\item $(ii)\Longrightarrow (i)$. Par l'absurde si $x_0\in\bar{F}$ il existe $(x_n)_n\in F^\N$ telle que $x_n\xrightarrow[n\infty]{}x_0$ par continuité de $f$ on aura $f(x_0)=\lim_{n\infty}f(x_n)=0$ ce qui est exclu par hypothèse. D'où $x_0\notin\bar{F}$.
\item $(i)\Longrightarrow (ii)$. Si $x_0\notin \bar{F}$ il existe $\delta>0,\ \norm{x-x_0}>\delta$ pour tout $x\in F$. Soit $F'$ le sev engendré par $F$ et $x_0$: $F'=F+\R x_0$. On pose $f(x+\lambda x_0)=\lambda$ si $x\in F,\ \lambda\in \R$. C'est une forme linéaire sur $F'$ et pour tout $y=x+\lambda x_0\in F'$:
\begin{itemize}[label=\textbullet,leftmargin=*]
\item $|f(y)|=|f(x+\lambda x_0)|=|\lambda|$
\item $\norm{y}=\norm{x+\lambda x_0}\underset{\lambda\neq 0}{=}|\lambda|\norm{\frac{x}{\lambda}+x_0}>|\lambda|\delta$ la dernière inégalité vient du fait que $-x/\lambda\in F$.
\end{itemize}
D'où si $\lambda\neq 0,\ |f(y)|=|\lambda|\leq\frac{1}{\delta}\norm{y}$ et si $y=0,\ |f(y)|=0\leq\frac{1}{\delta}\norm{y}$ donc pour tout $y\in F,\ |f(y)|=\leq\frac{1}{\delta}\norm{y}$ donc $f$ est linéaire continue et $\norms{f}\leq\frac{1}{\delta}$. De plus $f\mid_F=0$ et $f(x_0)=1$. Le théorème de Hahn-Banach donne alors : $f$ se prolonge à $E$ entier en $\tilde{f}$ linéaire continue vérifiant $\tilde{f}\mid_F=f\mid_F=0$ et $\tilde{f}(x_0)=f(x_0)=1\neq 0$.
\end{itemize}
\end{pr}
\subsection{Préliminaires géométriques}
\subsubsection{Hyperplans affines}
\begin{Def}
Soit $E$ un $\R$-evn. Un hyperplan affine de $E$ est un ensemble de la forme :
\[H=\{x\in E\ \mid\ f(x)=\alpha\}\]
où $f$ une forme linéaire non nulle sur $E$ et $\alpha\in \R$. On dit que $H$ est un hyperplan d'équation $[f=\alpha]$.
\end{Def}
\begin{Prop}
L'hyperplan d'équation $[f=\alpha]$ est fermée \ssi $f$ est continue. 
\end{Prop}
\begin{Def}
Soit $A,B\subset E$. On dit que l'hyperplan $H$ d'équation $[f=\alpha]$ sépare $A$ et $B$:
\begin{itemize}[label=\textbullet,leftmargin=*]
\item au sens large si: $\forall(a,b)\in A\times B,\ f(a)\leq \alpha\leq f(b)$.
\item au sens strict si $\exists\varepsilon>0,\ \forall (a,b)\in A\times B,\ f(a)\leq \alpha-\varepsilon<\alpha+\varepsilon\leq f(b)$
\end{itemize}
\end{Def}
\subsubsection{Rappels sur la convexité}
\begin{Def}
Soit $E$ un $\R$-ev et $C\subset E$. $C$ est une partie convexe \ssi: $\forall x,y\in C,\ [x,y]\subset C$.
\end{Def}
\begin{Def}
Soit $E$ un $\R$-ev et $C$ une partie convexe $f:C\to\R$ est convexe, \ssi \[\forall x,y\in C,\ \forall t\in[0,1], f((1-t)x+ty)\leq (1-t)f(x)+tf(y)\]
\end{Def}
\begin{Def}
Soit $E$ un $\R$-ev et $C$ une partie convexe de $E$ contenant $0_E$. On pose alors pour tout $x\in E$:
\[\rho(x)=\inf\{\alpha>0\ \mid\ x\in \alpha C\}\in[0,+\infty]\]
On appelle $\rho$ la jauge de $C$.
\end{Def}
\begin{Prop}
Si $C$ est un ouvert convexe de $E$ et $0_E\in C$:
\begin{enumerate}[label=(\roman*),leftmargin=*]
\item $\exists M>0,\ \forall x\in E,\ 0\leq\rho(x)\leq M\norm{x}$
\item $C=\{x\in E\ \mid \rho(x)<1\}$
\item $\rho$ est une fonction convexe
\end{enumerate}
\end{Prop}
\begin{pr}

\end{pr}
\subsection{Forme analytyique générale et forme géométrique du théorème de Hahn-Banach}
Dans la première version du théorème de Hahn-Banach on a supposé la forme linéaire $f$ continue: $\exists M\in\R,\ \forall x\in E\ |f(x)|\leq M\norm{x}$ et $\norms{f}=\inf_M$. De là, on a obtenue que son prolongement $\tilde{f}$ est continue de même norme\medbreak
\begin{thrm}{ de Hahn-Banach version analytique générale}
Soit $E$ un $\R$-evn et $F$ un sev de $E$ et $f$ une forme linéaire sur $F$. On suppose qu'il existe $\rho:E\to\R$ convexe tel que pour tout $x\in F,\ f(x)\leq \rho(x)$. Alors: $f$ peut être prolongée en une forme linéaire $\tilde{f}:E\to\R$ tel que $\forall x\in E,\ \tilde{f}(x)\leq\rho(x)$.
\end{thrm}
\begin{pr}

\end{pr}
\begin{Prop}
Soit $E$ un $\R$-evn et $C\subset E$ un ouvert convexe non vide, soit $x_0\in E\backslash C$. Alors il existe un hyperplan fermé qui sépare au sens large $\{x_0\}$ et $C$. C'est-à-dire il existe $f\in\L_c(E,\R)$ tel que $\forall x\in C,\ f(x)<f(x_0)$
\end{Prop}
\begin{pr}

\end{pr}

\begin{thrm}{}
Soit $A$ et $B$ deux parties convexes non vide disjointes de $E$. On suppose $A$ ouverte. Alors il existe un hyperplan fermé que sépare $A$ et $B$ au sens large.
\end{thrm}
