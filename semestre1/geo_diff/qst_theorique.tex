\documentclass[10pt]{article}
\usepackage{m1_geodiff}
%###### - Style de page 
\pagestyle{fancy}
\fancyhf{}
\chead{}
\cfoot{}
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\headrulewidth}{0pt}
\author{Y. Messaoudi}
\title{Géométrie Différentielle}
\date{}
\setlength{\parindent}{0ex}

\begin{document}
\maketitle
\tableofcontents\pagebreak
\section{Courbe et longueur, cas ou courbe de classe $C^1$. Paramètre de longueur naturel d'une courbe}
\subsection{Définitions}
\begin{definition}
	Une application $\gamma:I\to\R^n$ avec $I$ un intervalle fini ou pas est appelée courbe. Si $\gamma$ est $C^2$ la courbe est dite de classe $C^2$.
\end{definition}
\begin{definition}
	Soit $P:a=t_0<t_1<\cdots<t_m=b$ une subdivision, on note :
	\begin{equation}\label{l.brisee}
		l(\gamma,P)=\sum_{i=1}^m\norm{\gamma(t_i)-\gamma(t_{i-1})}
	\end{equation}
	la longueur d'une droite brisée. On définit alors la longueur de $\gamma$ ainsi:
	\begin{equation}
		\label{l}|\gamma|=\sup_{P\in\mathcal{P}([a,b])}l(\gamma,P)
	\end{equation}
	où $\mathcal{P}([a,b])$ l'ensemble des subdivisions de $[a,b]$. Si $|\gamma|<\infty$ alors elle est dite rectifiable.\medbreak
\end{definition}
\subsection{Preuves}
\begin{lemma}
	Si $\gamma:[a,c]\to\R^n$ avec $b\in]a,c[$ alors:
	\[|\gamma|=|\alpha|+|\beta|\]
	où $\alpha=\gamma|_{[a,b]}$ et $\beta=\gamma|_{[b,c]}$.
\end{lemma}
\begin{proof}
	Soit d'abord $P: a=t_0<t_1<\cdots<t_n=c$ une partition de $[a,c]$.\\
	Si $b=t_i\in P$ on a $l(\gamma,P)=l(\alpha,P_1)+l(\beta,P_2)$ où $P_1=P\cap[a,b]$ et $P_2=P\cap[b,c]$.\\
	Si $b\notin P$ on introduit une nouvelle subdivision : $P'=P\cup\{b\}$ et si $b\in]t_{i-1},t_i[$ on a :
	\[\norm{\gamma(t_{i-1})-\gamma(t_i)}\leq\norm{\gamma(t_{i-1})-\gamma(b)}+\norm{\gamma(b)-\gamma(t_i)}\]
	par inégalité triangulaire, donc $l(\gamma,P')\geq l(\gamma,P)$ et $l(\gamma,P')=l(\alpha,P_1)+l(\beta,P_2)$. D'où $l(\alpha,P_1)+l(\beta,P_2)\geq l(\gamma,P)$. Ce qui implique $|\gamma|\leq |\alpha|+|\beta|$.\\
	Pour l'autre sens, si $P_1\in\mathcal{P}([a,b]),\ P_2\in\mathcal{P}([b,c])$ alors $P=P_1\cup P_2\in \mathcal{P}([a,c])$ et $l(P,\gamma)=l(P,\alpha)+l(P,\beta)$ ce qui donne bien $|\gamma|\geq|\alpha|+|\beta|$.
\end{proof}
\begin{theorem}{}
	Soit $\gamma:[a,b]\to\R^n$ une courbe de classe $C^1$ (courbe lisse). Alors $\gamma$ est rectifiable et:
	\[|\gamma|=\int_a^b\norm{\gamma'(t)}\dt\]
\end{theorem}
\begin{proof}
	Soit $P=\{a=t_0<t_1<\cdots<t_m=b\}\in\mathcal{P}([a,b])$ une subdivision. Alors:
	\[l(\gamma,P)=\sum_{i=1}^m\norm{\gamma(t_i)-\gamma(t_{i-1})}=\sum_{i=1}^m\norm{\int_{t_{i-1}}^{t_i}\gamma'(t)\dt}\leq\sum_{i=1}^m\int_{t_{i-1}}^{t_i}\norm{\gamma'(t)}\dt=\int_a^b\norm{\gamma'(t)}\dt<+\infty\]
	Car $\gamma'$ bornée puisque $C^0$. Donc $\norm{\gamma}\leq \int_a^b\norm{\gamma'(t)}\dt$.\\
	Soit $\lambda(t)=|\gamma_{[a,t]}|$ la longueur de $\gamma([a,t])$ et $\mu(t)=\int_a^t\norm{\gamma'(t)}\dt$ pour $t\in [a,b[$. Soit $h>0$ tel que $a\leq t<t+h\leq b$, puisque toute subdivision de $[t,t+h]$ contient des points extrémaux, par inégalité triangulaire on a :
	\[\norm{\gamma(t+h)-\gamma(t)}\leq |\gamma|_{[t,t+h]}|=|\gamma|_{[a,t+h]}|-|\gamma|_{[a,t]}|=\lambda(t+h)-\lambda(t)\]
	Donc :
	\[\frac{\norm{\gamma(t+h)-\gamma(t)}}{h}\leq \frac{\lambda(t+h)-\lambda(t)}{h}=\frac{|\gamma|_{[t,t+h]}|}{h}\leq \frac{1}{h}\int_t^{t+h}\norm{\gamma'(t)}\dt=\frac{\mu(t+h)-\mu(t)}{h}\]
	En passant à la limite on a alors: $\norm{\gamma'(t)}\leq\lambda'(t)\leq\mu'(t)$. Et en intégrant :
	\[\int_a^b\norm{\gamma'(t)}\dt\leq \lambda(b)-\lambda(a)\leq \mu(b)-\mu(a)=\int_a^b\norm{\gamma'(t)}\dt\]
	d'où l'égalité : $\lambda(b)=|\gamma|=\int_a^b\norm{\gamma'(t)}\dt$.
\end{proof}
\begin{definition}
	La fonction $s:[a,b]\to[0,|\gamma|]$ défini comme :
	\[s(t)=\int_a^t\norm{\gamma'(t)}\dt\]
	est appelé paramètre naturel de $\gamma$. Elle est croissante et donne une nouvelle paramétrisation de la courbe appelée naturelle:
	\[\bar{\gamma}(s):=\gamma(t(s))\]
	où $t(s)$ est la fonction réciproque de $s(t)$. On aura alors:
	\[s'(t)=\norm{\gamma'(t)}\ \d s=\norm{\gamma'(t)}\dt\]
\end{definition}
\textbf{Notation.} On notera souvent une courbe : $r:t\to\R^n,\ t\longmapsto(x_1(t),\dots,x_n(t))$.\medbreak
\begin{proposition}
	Soit $s\longmapsto r(s)$ une courbe de classe $C^2$ paramétrée naturellement, alors on a :
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item $\norm{\der{r}{s}}=1$
		\item $\scal{\der{r}{s}}{\der{^2 r}{s^2}}=0$
	\end{enumerate}
	les vecteurs d'accéleration et de vitesse sont orthogonaux si le paramètre est naturel.
\end{proposition}
\begin{proof}
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item On a $\der{r}{s}=\der{r}{t}\cdot\der{t}{s}=\frac{\der{r}{t}}{\der{s}{t}}$ par théorème de dérivée de la fonction inverse, de plus $\norm{s'(t)}=\norm{r'(t)}$ d'où $\norm{\der{r}{s}}=1$
		\item On a de i) $\scal{\der{r}{s}}{\der{r}{s}}=1$ en dérivant on a $2\scal{\der{r}{s}}{\der{^2 r}{s^2}}=0$ d'où le résultat.
	\end{enumerate}
\end{proof}

\section{Courbe géodésique, équivalence avec droite affine}
\begin{definition}
	Soit $(X,d)$ un espace métrique, une courbe $\gamma:I\to X$ est dite géodésique si $\forall t_0\in I$ il existe $\delta_{t_0}>0$ tel que $\gamma|_{]t_0-\delta,t_0+\delta[}$ soit une isométrie:
	\[\forall t_1,t_2\in ]t_0-\delta,t_0+\delta[,\ |t_2-t_1|=\d(\delta(t_1),\delta(t_2))\]
	Faire attention, le $\delta$ dépend bien de $t_0$ et $\gamma$ est une isométrie locale. Dans le cas euclidien, $X=\R^n$, une géodésique est forcément une droite affine et l'isométrie est globale.
\end{definition}
\begin{theorem}{ (constante de Lebesgue)}
	Soit $(X,d)$ espace métrique compact. On suppose $(U_i)_{i\in I}$ recouvrement ouvert de $X$. Alors il existe $r>0,\forall x\in X,\exists i\in I,\ \ \B(x,r)\subset U_i$. Qui se dit aussi : $\forall A\subset X,\ \text{diam}(A)<r,\ \exists i\in I,\ A\subset U_i$.
\end{theorem}
\begin{proof}
	Par l’absurde : pour tout $r > 0$, il existe un $x\in X$ tel que $\B(x,r)$ ne soit contenue dans aucun $U_i$. Pour $r =1/(n+1)$, on trouve un $x_n$ tel que $\B(x_n, 1/(n+1))$ ne soit contenue dans aucun $U_i$. On considère une sous-suite $(x_{\varphi(n)})$ convergente vers un $x\in X$. Il existe un $i$ tel que $x\in U_i$, et donc un $\varepsilon>0$ tel que $\B(x,\varepsilon)\subset U_i$. Il existe un $k_0$ tel que $\d(x_{\varphi(n)},x)<\varepsilon/2$ si $k\geq k_0$.
	Il existe un $k_1$ tel que $1/(\varphi(n)+1)< \varepsilon/2$ si $k\geq k_1$. Si $k =\max(k_0,k_1)$, alors $z\in\B(x_{\varphi(n)},1/(\varphi(n)+1))\Longrightarrow\d(z,x_{\varphi(n)})+\d(x_{\varphi(n)},x)<\varepsilon$, et donc $\B(x_{\varphi(n)},1/(\varphi(n)+1))\subset \B(x,\varepsilon)\subset U_i$, contradiction.
\end{proof}
\begin{proposition}
	Soit $x,y,z$ trois points de $\R^n$ on a équivalence entre:
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item $x,y,z$ sont colinéaires i.e. $y=x+(z-x)t$ pour $t\in [0,1]$
		\item $\norm{x-z}=\norm{x-y}+\norm{y-z}$
	\end{enumerate}
\end{proposition}
\begin{theorem}{}
	Un courbe $\gamma: I\to \R^n$ est une géodésique $\iff$ elle est une droite affine:
	\[\gamma(t)=at+b \text{ avec }a\in\R^n,\ \norm{a}=1\]
\end{theorem}
\begin{proof}
	\begin{itemize}[label=\textbullet,leftmargin=*]
		\item $\Longleftarrow$ C'est direct, on a :
		\[\norm{\gamma(t_2)-\gamma(t_1)}=\norm{a}|t_1-t_2|=|t_1-t_2|\]
			\item $\Longrightarrow$ Par hypothèse:
		\[\forall x\in I,\ \exists\varepsilon_{x}>0,\ \gamma|_{]x-\varepsilon_{x},x+\varepsilon_{x}[}\text{ soit isométrie}\]
		On a alors en posant $U_{x}=]x-\varepsilon_{x},x+\varepsilon_{x}[$, $(U_{x})_{x\in I}$ est un recouvrement d'ouvert de $I$. On pose alors $\delta$ le nombre de Lebesgue de ce recouvrement:
		\[\forall A\subset I,\ \text{diam}(A)\leq\delta,\ \exists x\in I,\ A\subset U_x\]
		Fixons une subdivision $P: a=t_0<t_1<\cdots<t_m=b$ tel que $|t_{i+1}-t_{i-1}|\leq \delta$. Démontrons par récurrence sur $n$ la propriété: $\mathcal{P}(n): \gamma|_{[t_0,t_n]}$ est une droite affine.
		\begin{itemize}[label=\textbullet,leftmargin=*]
			\item Pour $n=1$. Puisque $t_1-t_0<t_2-t_0\leq\delta$ on a $\gamma|_{[t_0,t_1]}$ isométrique. Alors pour tout $t\in [t_0,t_1]$ on a:
			\[\norm{\gamma(t_0)-\gamma(t)}+\norm{\gamma(t)-\gamma(t_1)}=t_1-t_0=\norm{\gamma(t_1)-\gamma(t_0)}\]
			donc, les trois points sont alignés, ils appartiennent à la même droite:
			\[\frac{\gamma(t)-\gamma(t_0)}{t-t_0}=\frac{\gamma(t_1)-\gamma(t_0)}{t_1-t_0}\]
			donc finalement:
			\begin{align*}
				\gamma(t)&=\frac{\gamma(t_1)-\gamma(t_0)}{t_1-t_0}\times t+\gamma(t_0)-t_0\times\frac{\gamma(t_1)-\gamma(t_0)}{t_1-t_0}:=at+b
			\end{align*}
			avec $a=\frac{\gamma(t_1)-\gamma(t_0)}{t_1-t_0}$ et $b=\gamma(t_0)-at_0$. Donc $\mathcal{P}(1)$ vraie.
			\item Supposons $n\geq 1$ tel que $\gamma|_{[t_0,t_{n}]}$ droite affine. Puisque $t_{n+1}-t_{n-1}\leq \delta$ on a $\gamma|_{[t_{n-1},t_{n+1}]}$ isométrique donc pour tout point $t\in[t_{n-1},t_{n+1}]$ on a :
			\[\norm{\gamma(t)-\gamma(t_{n-1})}+\norm{\gamma(t)-\gamma(t_{n})}=t_{n}-t_{n-1}=\norm{\gamma(t_{n})-\gamma(t_{n-1})}\]
			Donc les points sont alignés et on a:
			\[\forall t\in [t_{n-1},t_{n+1}],\ \frac{\gamma(t)-\gamma(t_{n-1})}{t-t_{n-1}}=\frac{\gamma(t_n)-\gamma(t_{n-1})}{t_n-t_{n-1}}\]
			L'hypothèse de récurrence nous dit que $\gamma(t_n)=at_n+b$ et $\gamma(t_{n-1})=at_{n-1}+b$ donc:
			\[\frac{\gamma(t)-\gamma(t_{n-1})}{t-t_{n-1}}=a\]
			et finalement:
			\[\gamma(t)=a(t-t_{n-1})+at_{n-1}+b=at+b\]
			Donc $\mathcal{P}(n+1$ vraie.
		\end{itemize} 
		Par récurrence, on a que $\mathcal{P}(m)$ est vraie donc $\gamma$ est une droit affine sur $[a,b]$.\qedhere
	\end{itemize}
\end{proof}
\section{Courbure, formules de Frénet pour $\R^2$ et $\R^3$}
\subsection{Dans $\R^2$}
\begin{definition}
		La norme du vecteur accélération $k(s)=\norm{r''(s)}$ par rapport au paramètre naturel $s$ est appelée courbure de la courbe $r$.
		La quantité $R(s)=\frac{1}{k(s)}$ est appelée rayon de la courbure.
\end{definition}
\begin{definition}
	Le vecteur $n(t)=\frac{r''(t)}{\norm{r''(t)}}$ de norme 1 est appelé vecteur normal. On a $\scal{n(t)}{r'(t)}=0$ d'où $n$ orthogonal à $r'$. Donc en particulier on a $r''(s)=k(s)n(s)$.
\end{definition}
\begin{theorem}{}
	Soit $s\longmapsto r(s)\in\R^2$ une courbe plane de classe $C^3$ paramétrée naturellement alors les formules de Frénet ont lieu:
	\begin{align}\tag{1}
	\der{}{s}\matrice{v\\n}=\matrice{0&k\\-k&0}\matrice{v\\n}
	\end{align}
\end{theorem}
\begin{proof}
	\begin{itemize}[label=\textbullet,leftmargin=*]
		\item La première est un corollary direct des définitions.
		\item On a $\scal{n(s)}{n(s)}=1$ donc $\scal{n'(s)}{n(s)}=0$ donc $n(s)\perp n'(s)$, de plus $n(s)\perp r'(s)$ donc puisque qu'on est dans $\R^2$ on a $r'(s)$ et $n'(s)$ colinéaires, on a donc $\beta:[0,|\gamma|]\to\R$ tel que $n'(s)=\beta(s)r'(s)$. En dérivant finalement l'égalité $\scal{n(s)}{r'(s)}=0$ on a :
		\begin{align*}
			0&=\scal{n'(s)}{r'(s)}+\scal{n(s)}{r''(s)}=\beta(s)\scal{r'(s)}{r'(s)}+\scal{n(s)}{n(s)k(s)}=\beta(s)+k(s)
		\end{align*}
		la dernier égalité venant du fait que $\scal{n(s)}{n(s)}=\scal{r'(s)}{r'(s)}=1$. Et donc finalement $n'(s)=-k(s)r'(s)$.
	\end{itemize}
\end{proof}
\subsection{Dans $\R^3$}
Soit $r:t\longmapsto r(t)=(x(t),y(t),z(t))$ de classe $C^3$. Le paramètre naturel:
\[s(t)=\int_a^t\sqrt{\dot{x}(x)+\dot{y}(x)+\dot{z}(x)}\dx\]
et en reparamétrisant de façon naturelle on a $v(s)=r'(s)$ et $w(s)=v'(s)=r''(s)$. On aura $\scal{v(s)}{w(s)}=0$ et en posant $k(s)=\norm{w(s)}$ on a la première formule de Frénet $w(s)=n(s)k(s)$.\medbreak
\begin{definition}
	On définit le vecteur binormal par :
	\[b(s):=v(s)\wedge n(s)\]
	Par définition du produit vectoriel, on a $b\perp v$, $b\perp n$ et $(v,n,b)$ forment un repère positif qu'on appelle repère de Frénet(ou encore $\det(v,n,b)>0$). De plus $\norm{b}=\norm{v}\norm{n}\sin(\widehat{v,n})=\norm{v}\norm{n}$ puisque $v\perp n$. On rappelle aussi que si $(e_1,e_2,e_3)$ base orthonormée on a :
	\[b=v\wedge n=\determinant{e_1&e_2&e_3\\v_1&v_2&v_3\\n_1&n_2&n_3}\]
\end{definition}

\begin{lemma}
	Soit $t\longmapsto a_{ij}(t)$ fonction matricielle  tel que pour tout $t$ la matrice $A(t)=(a_{ij}(t))_{ij}$ soit orthogonale et $A(0)=I_n$. Si $A$ est différentiable alors $B=A'(0)$ est antisymétrique. On rappelle :
	\[A\in O_n(\R)\iff A^TA=AA^T=I_n\iff \sum_{k=1}^na_{ik}a_{jk}=\delta_{ij}\]
\end{lemma}
\begin{proof}
	Pour $t\in ]-a,a[$ on a $\sum_{k=1}^na_{ik}a_{jk}=\delta_{ij}$ donc en dérivant selon $t$ on a :
	\[\sum_{k=1}^n\dot{a}_{ik}a_{jk}+a_{ik}\dot{a}_{jk}=0\]
	Puis en évaluant en $0$, puisque $A(0)=I_n$ on a :
	\[\sum_{k=1}^n\dot{a}_{ik}\delta_{jk}+\delta_{ik}\dot{a}_{jk}=0\iff \dot{a}_{ij}(0)=-\dot{a}_{ji}(0)\]
	Donc $A'(0)=-A'(0)^T$ d'où le lemma.
\end{proof}
\begin{theorem}{ (formules de Frénet)}
	Soit $r:s\longmapsto r(s)$ une courbe paramétrée naturellement de classe $C^3$ alors les formules suivantes ont lieux:
	\[\der{}{s}\matrice{v\\n\\b}=\matrice{0&k&0\\-k&0&-\kappa\\0&\kappa&0}\matrice{v\\n\\b}\]
	Ou le coefficient $\kappa$ est appelé torsion de courbe.
\end{theorem}
\begin{proof}
	On pose $e_1(s)=v(s),\ e_2(s)=n(s),\ e_3(s)=b(s)$, alors $(e_i(s+\Delta s))_i$ et $(e_i(s))_i$ forment des bases orthonormées de $\R^3$.\\
	On note $(a_{ij}(s,\Delta s))_{ij}$ la matrice orthogonale de passage entre les bases $(e_i(s))_i$ et $(e_i(s+\Delta s))_i$. On a alors $e_i(s+\Delta s)=\sum_{j=1}^3a_{ij}(s,\Delta s)e_j(s)$. Du lemma, on a $b_{ij}:=a'_{i,j}(s,0)$ est antisymétrique et on a pour tout $s$:
	\[\frac{e_i(s+\Delta s)-e_i(s)}{\Delta s}=\sum_{j=1}^3\frac{(a_{ij}(s,\Delta s)-a_{ij}(s,0))}{\Delta s}e_j(s)\xrightarrow[\Delta s\tto 0]{}\sum_{j=1}^3b_{ij}(s)e_j(s)\]
	En notant $B=\matrice{0&b_{12}&b_{13}\\-b_{12}&0&-b_{23}\\-b_{13}&b_{23}&0}$ l'égalité précédente s'écrit:
	\[\der{}{s}\matrice{v\\n\\b}=\matrice{0&b_{12}&b_{13}\\-b_{12}&0&-b_{23}\\-b_{13}&b_{23}&0}\matrice{v\\n\\b}\]
	de la première formule de Frénet : $\der{v}{s}=kn$ on déduit $k=b_{12}$ et $0=b_{13}$ et on pose $b_{23}=\kappa$. d'où le résultat.
\end{proof}


\section{Surface $S$ sur $\R^3$ de trois manières (paramétriques, explicite et implicite). Métrique sur $S$, espace tangent et 1ère forme quadratique. Introduire 2ème forme quadratique et formule de courbure de Gauss}
\begin{definition}
	On peut définir une surface $S\subset \R^3$ sous 3 formes:
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item Sous forme paramétrique:
		\[r:(u,v)\in D\subset \R^2\longmapsto r(u,v)=(x(u,v),y(u,v),z(u,v))\in\R^3\]
		On note alors $S=r(D)=\{(x(u,v),y(u,v),z(u,v))\mid (u,v)\in D\}$. $r$ est supposée de classe $C^3$.
		\item Sous forme explicite:
		\[f:(x,y)\in D\longmapsto z=f(x,y)\in \R\]
		alors la surface est $S=\{(x,y,f(x,y))\mid (x,y)\in D\}$. C'est en fait un cas particulier du premier cas, où $u=x,\ v=y$ et on a $r(u,v)=(u,v,f(u,v))$.
		\item Sous forme implicite
		\[F(x,y,z)=0\]
		où $F:R^3\to\R$ est une fonction de classe $C^2$. Remarqous que par le théorème des fonctions implicites si :
		\[\grad(F)=\left(\derp{F}{x},\derp{F}{y},\derp{F}{z}\right)\neq 0\]
		au point $(x_0,y_0,z_0)$ alors $\derp{F}{z}(x_0,y_0,z_0)\neq 0$ donc il existe un voisinage $U_0$ de $(x_0,y_0)$ tel que $z=f(x,y)$ et donc $F(x,y,f(x,y))=0$ pour tout $(x,y)\in U_0$. On se ramène donc au cas 2.
	\end{enumerate}
\end{definition}
\begin{definition}
	Soit une surface $S$ définit paramétriquement:
	\[(u,v)\in\R^2\longmapsto r(u,v)\in\R^3\]
	Une courbe $\gamma:I\to\R^3$ appartient à la surface $S$ si:
	\[\forall t\in I,\ \gamma(t)=r(u(t),v(t))=(x(u(t),v(t)),y(u(t),v(t)),z(u(t),v(t)))\]
	On a alors:
	\[\tag{1}\gamma'(t)=(x_u\dot{u}+x_v\dot{v},y_u\dot{u}+y_v\dot{v},z_u\dot{u}+z_v\dot{v})\]
	est un vecteur tangent à la surface défini par une courbe sur $S$. on aura aussi $\gamma'(t)=r_u\dot{u}+r_v\dot{v}$ donc d'après (1):
	\[\tag{1'}r_u=(x_u,y_u,z_u)\quad r_v=(x_v,y_v,z_v)\]
	donc chaque vecteur tangent à $S$ est une combinaison linéaire de vecteur $r_u$ et $r_v$. On peut l'écrire:
	\[\tag{2}T_{(u,v)}S=\Vect(r_u,r_v)\]
	où $T_{(u,v)}S$ est l'espace tangent à $S$ au point $(u,v)$.\medbreak
	Un surface $S$ est dite non singulière au point $(u,v)$ si les vecteurs $r_u$ et $r_v$ sont libres au point $(u,v)$ ou encore si:
	\[\rg\matrice{x_u&y_u&z_u\\x_v&y_v&z_v}=2\]
\end{definition}
\textbf{exemple.} Soit la surface suivante (c'est un hélicoïde) :
\[r(u,v)=(v\cos u,v \sin u,\ k u)\quad k\neq 0\]
On a $r_u=(-v\sin u,v\cos u,k)$ et $r_v=(\cos u,\sin u,0)$ on a :
\[\rg\matrice{-v\sin u&v\cos u&k\\ \cos u&\sin u&0}=1+\rg\matrice{-\sin u,\cos u}=2\]
Donc l'hélicoide est non singulière en tout point $(u,v)$. On rappelle qu'un plan passant par $(x_0,y_0,z_0)$ est donnée par l'équation :
\[A(x-x_0)+B(y-y_0)+C(z-z_0)=0\]
où $m=(A,B,C)$ est le vecteur normal au point $(x_0,y_0,z_0)$. On a :
\[m=r_u\wedge r_v=\determinant{e_1&e_2&e_3\\-v \sin u&v \cos u&k\\ \cos u&\sin u&0}=-k\sin ue_1+k\cos u e_2-v e_3\]
Donc finalement l'équation du plan tangent $T_{(u,v)}S$ est :
\[-k\sin u(x-v\cos u)+k\cos u(y-v\sin u)-v(z-ku)=0\]\medbreak
\subsection{1ère forme fondamentale}
Soit $S\subset \R^3$ un surface donnée paramétriquement:
\[(u,v)\longmapsto r(u,v)\]
On suppose qu'elle est non singulière au point $(u,v)$. Soit $\gamma:t\in[a,b]\to S$ une courbe sur $S$. On a :
\[l(\gamma)=\int_a^b\sqrt{\dot{x}^2+\dot{y}^2+\dot{z}^2}\dt\]
Donc on aura 
\[\dot{s}^2=\dot{x}^2+\dot{y}^2+\dot{z}^2=(x_u\dot{u}+x_v\dot{v})^2+(y_u\dot{u}+y_v\dot{v})^2+(z_u\dot{u}+z_v\dot{v})^2=(x_u^2+y_u^2+z_u^2)\dot{u}^2+2(x_ux_v+y_uy_v+z_uz_v)\dot{u}\dot{v}+(x_v^2+y_v^2+z_v^2)\dot{v}^2\]
et donc :
\begin{equation}\tag{3}
	\left\{
		\begin{array}{l}
			\d s^2=E\d u^2+2F\d u\d v+G\d v^2\\
			E=x_u^2+y_u^2+z_u^2=\norm{r_u}^2\\
			F=x_ux_v+y_uy_v+z_uz_v=\scal{r_u}{r_v}\\
			G=x_v^2+y_v^2+z_v^2=\norm{r_v}^2
		\end{array}
	\right.
\end{equation}
\begin{definition}
	La forme quadratique $\d s^2$ donnée par (3) est appelée la 1ère forme fondamentale sur $S$ et on la note $\I$ ou $g$. On peut la réécrire:
	\[g=\sum_{i,j}^2g_{ij}\d x_i\d x_j\]
	où $g_{g_12}=g_{21}=F\ g_{11}=E$ et $g_{22}=G$.
\end{definition}
\begin{proposition}
	La forme $g$ est positive et symétrique en tout point non-singulier.
\end{proposition}
\subsubsection*{Formule implicite}
Soit $S$ une surface donnée par $z=f(x,y)$ donc $r(x,y)=(x,y,f(x,y))$ on a : $r_x=(1,0,f_x)$ et $r_y=(0,1,f_y)$ donc on aura :$g=E\d x^2+2F\d x\d y+H\d y^2$ où $E=\norm{r_x}^2=1+f_x^2,\ G=\norm{r_y}^2=1+f_y^2$ et $F=\scal{r_x}{r_y}=f_xf_y$.
\subsubsection{Formule explicite}
Soit une surface définit par $F(x,y,z)=0$ on a alors $F_x\dot{x}+F_y\dot{y}+F_z\dot{z}=0$ on suppose que $F_z(x_0,y_0,z_0)\neq 0$ on a alors, puisque $F_x\d x+F_y\d y+F_z\d z=0$, $\d z=-\frac{F_x}{F_z}\d x-\frac{F_y}{F_z}\d y$. 
S'ensuit, puisque $\d s^2=\d x^2+\d y^2+\d z^2=\left(1+\frac{F_x^2}{F_y^2}\right)\d x^2+2\frac{F_x F_y}{F_z^2}\d x\d y+\left(1+\frac{F_y^2}{F_x ^2}\right)\d y^2$ et on pose alors :
\begin{equation}\tag{5}
	\left\{
	\begin{array}{l}
		\d s^2=E\d u^2+2F\d u\d v+G\d v^2\\
		E=1+\frac{F_x^2}{F_y^2}\\
		F=\frac{F_x F_y}{F_z^2}\\
		G=1+\frac{F_y^2}{F_x ^2}
	\end{array}
	\right.
\end{equation}
\subsection{2ème forme fondamentale}
\subsubsection*{Forme paramétrique}
Soit $(u,v)\longmapsto r(u,v)$ une surface $S$ non-régulière en $(u,v)$. Les vecteurs $r_u=(x_u,y_u,z_u)$ et $r_v=(x_v,y_v,z_v)$ engendrent l'espace tangent $T_{(u,v)}S$. Le vecteur normal $m$ est donnée par :
\[m=\frac{r_u\wedge r_v}{\norm{r_u\wedge r_v}}\]
On a $\dot{r}=r_u\dot{u}+r_v\dot{v}$ et $\ddot{r}=r_u\ddot{u}+(r_{uu}\dot{u}+r_{uv}\dot{v})\dot{u}+r_v\ddot{v}+(r_{vv}\dot{v}+r_{vu}\dot{u})\dot{v}=r_u\ddot{u}+r_v\ddot{v}+2r_{uv}\dot{v}\dot{u}+r_{uu}\dot{u}^2+r_{vv}\dot{v}^2$. En prenant alors le produit scalaire du vecteur avec $m$, puisque $\scal{r_u}{m}=\scal{r_v}{m}=0$ on a :
\[\scal{\ddot{r}}{m}=\scal{r_{uu}}{m}\dot{u}^2+2\scal{r_{uv}}{m}\dot{u}\dot{v}+\scal{r_{vv}}{m}\dot{v}^2\]
On pose alors:
\[\tag{2}\II=q(u,v)=\scal{\ddot{r}}{m}\d t^2=L\d u^2+2M\d u\d v+N\d v^2\]
avec :
\begin{equation}
	\left\{
		\begin{array}{l}
			L=\scal{r_{uu}}{m}\\
			M=\scal{r_{uv}}{m}\\
			N=\scal{r_{vv}}{m}
		\end{array}
	\right.
\end{equation}
La forme (2) est appelée la 2ème forme fondamentale sur $S$.
\subsubsection{Forme explicite}
Soit $r(x,y)=(x,y,f(x,y))$ on a $r_x=(1,0,f_x)$ $r_y=(0,1,f_y)$ $r_{xx}=(0,0,f_{xx})$ $r_{xy}=(0,0,f_{xy})$ et $r_{yy}=(0,0,f_{yy})$. Alors:
\[m=\frac{\determinant{e_1&e_2&e_3\\1&0&f_x\\0&1&f_y}}{\norm{m}}=\frac{(-f_x,-f_y,1)}{\sqrt{1+f_x^2+f_y^2}}\]
Et donc $L=\scal{r_{xx}}{m}=\frac{f_{xx}}{\sqrt{1+f_x^2+f_y^2}}$ $M=\scal{r_{xy}}{m}=\frac{f_{xy}}{\sqrt{1+f_x^2+f_y^2}}$ et $N=\scal{r_{yy}}{m}=\frac{f_{y}}{\sqrt{1+f_x^2+f_y^2}}$
\subsubsection*{Lien entre les formes fondamentales}
Soit $\gamma: I\to S$ une courbure sur une surface, on a :
\[\scal{ddot{r}}{m}=L\dot{u}^2+2M\dot{u}\dot{v}+N\dot{v}^2\]
En changeant la variable $t$ par $s$ on a :
\[\scal{r''(s)}{m}\d s^2=L\d u^2+2M\d u\d v+N\d v^2\]
et en remplaçant par la 1ère formule de Frénet: $r''(s)=k(s)n(s)$ et en remarquant que $\I=\d s^2$:
\[k\scal{n(s)}{m(s)}=k\cos \theta=\frac{L\d u^2+2M\d u\d v+N\d v^2}{\d s^2}=\frac{\II}{\I}\]
où $\theta=\widehat{m,n}$.
\subsubsection*{Courbure de surface}
Soit $g(x,y)$ et $q(x,y)$ 2 formes fondamentales données par les matrices:
\[A=\matrice{E&F\\F&G}\quad B=\matrice{L&M\\M&N}\]
On sait que $\det(A)>0$.\medbreak
\begin{definition}
	Les valeurs propres de la paire $(A,B)$ qui sont les racines $\lambda_1$ et $\lambda_2$ de :
	\[\det(B-XA)=\det(A)\times\det(A^{-1}B-XI_2)\]
	sont appelées courbures principale. La valeur $K=\lambda_1\cdot\lambda_2$ est la courbure de Gauss et $\lambda_1+\lambda_2$ est la courbure moyenne. Les vecteurs propres $X_{\lambda_i}$ sont appelés vecteurs directeurs et ils vérifient:
	\[(B-\lambda_iA)X_{\lambda_i}=0\]
\end{definition}
\begin{theorem}{}
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item Si $(u,v)\longmapsto r(u,v)$ est une surface donnée sous la forme paramétrique alors:
		\[K=\lambda_1\cdot\lambda_2=\frac{\det B}{\det A}\text{ et }\lambda_1+\lambda_2=\Tr(A^{-1}B)\]
		\item Si $z=f(x,y)$ est une forme explicite, alors :
		\[K=\frac{f_{xx}f_{yy}-f_{xy}^2}{(1+f_x^2+f_y^2)^2}\]
	\end{enumerate}
\end{theorem}
\begin{proof}\ 
	\begin{enumerate}[label=(\roman*),leftmargin=*]
	\item On a :
	\[\det(A^{-1}B-XI_2)=X^2-\Tr(A^{-1}B)X+\det(A^{-1}B)=(X-\lambda_1)(X-\lambda_2)\]
	d'où les 2 formules.
	\item On a dèja vu : $L=\scal{r_{xx}}{m}=\frac{f_{xx}}{\sqrt{1+f_x^2+f_y^2}}$ $M=\scal{r_{xy}}{m}=\frac{f_{xy}}{\sqrt{1+f_x^2+f_y^2}}$ $N=\scal{r_{yy}}{m}=\frac{f_{y}}{\sqrt{1+f_x^2+f_y^2}}$ et $E=\norm{r_x}^2=1+f_x^2,\ G=\norm{r_y}^2=1+f_y^2$ et $F=\scal{r_x}{r_y}=f_xf_y$. Donc:
	\begin{align*}
		\lambda_1\cdot\lambda_2=\frac{LN-M^2}{EG-F^2}=\frac{f_{xx}f_{yy}-f_{xy}^2}{(1+f_x^2+f_y^2)^2}
	\end{align*}
	\end{enumerate}
\end{proof}
\section{Rappels de calcul différentiables}
\begin{definition}
	Une application $f:U\to F$ où $U$ est un ouvert de $E$ est dite differentiable au point $a\in U$ si:
	\[ f(a+h)-f(a)=Lh+o(\norm{h})\]
	où $L\in\L(E,F)$. On note alors $L$ $\d f_a$ ou encore $Df_a,\ D_af,\d_af,\d f(a)$. On peut réecrire:
	\[f(a+h)-f(a)=\d f_ah+o(\norm{h})\]
\end{definition}

\begin{definition}
	Une application $f:U\subset E\to F$ est différentiable (ou lisse) sur $U$ si $\d f_a$ existe pour tout $a\in U$. On dit qu'elle est de classe $C^1$ si $\d f_a:U\to F$ est continue en tout $a\in U$ et elle est de classe $C^k$ si $\d^k f_a$ définie par récurrence comme $\d(\d^{k-1}f)_a$ est continue sur $U$.
\end{definition}
\subsection*{Représentation de $\d f_a$ en coordonnées}
Fixons $(e_1,\dots,e_n)$ et $(e_1',\dots,e_m')$ bases de $E$ et $F$. L'application $\d f_x$ est donnée par sa matrice de Jacobi:
\[\d f_x=\Jac f_x=\left(\derp{f_i}{x_j}\right)_{\substack{1\leq i\leq m\\ 1\leq j\leq n}} =\matrice{
\derp{f_1}{x_1}&\cdots&\derp{f_1}{x_n}\\
\vdots&\ddots&\vdots\\
\derp{f_m}{x_1}&\cdots&\derp{f_n}{x_m}}
\]
Si $n=m$ son déterminant est noté $\determinant{\Jac f_x}$. On a :
\[\d f_x h=\left(\derp{f_i}{x_j}h_j\right)_{i=1}^m=\sum_{j=1}^n\derp{f}{x_j}h_j\]
où $\derp{f}{x_j}=\left(\derp{f_1}{x_j},\dots,\derp{f_m}{x_j}\right)$.
\subsection{Théorèmes importants}
\begin{theorem}{}
	Soient $E,F,H$ des espaces linéaires de dimensions finies et $f:U\subset E\to F$ et $g:V\subset F\to H$ deux applications différentiable en $a\in U$ et $f(a)$ respectivement, alors $g\circ f$ est differentiable et on a :
\[\d(g\circ f)_a=\d g_{f(a)}\circ \d f_a\]
\end{theorem}
\begin{definition}
	Une application $f:U\subset R^n\to V\subset \R^m$ est un difféomorphisme si $f$ est bijective, différentiable et de réciproque $f^{-1}$ différentiable sur $V$.
\end{definition}
\begin{proposition}
	Si $f^{-1}$ est différentiable en $f(a)$ alors:
	\[\d f^{-1}_{f(a)}=(\d f_a)^{-1}\]
\end{proposition}
\begin{theorem}{ sur les fonctions implicites} 
	Soient $U\subset R^n$ et $V\subset \R^m$ et $f:U\times V\to \R^m$ une application de classe $C^k$. On suppose que:
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item $f(x_0,y_0)=0$ pour un $(x_0,y_0)\in U\times V$
		\item $\derp{f}{y}(x_0,y_0)$ est inversible i.e $\det\left(\derp{f}{y}(x_0,y_0)\right)\neq 0$
	\end{enumerate}
	Alors il existe $V'\subset V$ et $U'\subset U$ et $g:V'\to U'$ de classe $C^k$ tel que $y=g(x)$ : $f(x,g(x))=0$ pour $x\in U',\ y\in V'$. Ou bien:
	\[\{(x,y)\in U'\times V'\mid f(x,y)=0\}=\{(x,g(x)\mid x\in V'\}\]
	est le graphe de $g$.
\end{theorem}
\begin{theorem}{ d'inversion locale}
	Soit $f:U\to V$ une fonction de classe $C^1$ et $\d f_{x_0}$ un isomorphisme en $x_0\in U$. Alors il existe $U'\subset U, \ V'\subset V$ tels que :$f^{-1}:V'\to U'$ est de classe $C^1$ et $\d f^{-1}_{f(x)}=(\d f_x)^{-1}$ pour $x\in U'$.
\end{theorem}
\section{Théorème du rang constant pour applications différentiables et preuve. Définition immersion et submersions, obtenir corollaires du théorème du rang constant pour ces applications}
\begin{lemma}
	Si $C=AB$ où $B:\R^n\to \R^n$ un isomorphisme linéaire et $A\R^n\to\R^m$ est linéaire de rang $r$, alors $\rg(C)=r$.
\end{lemma}
\begin{theorem}{ du rang constant}
	Soient $U,V$ des ouverts de $\R^n$ et de $\R^m$ et $f:U\to V$ une application de classe $C^k$. On suppose que $\rg(\d f_x)=r$ sur $U$. Alors il existe des ouverts $U'\subset U$ et $V'\subset V$ et des difféomorphismes: $\varphi: U'\to U'$ et $\psi: V'\to V'$ tels que :
	\[\psi\circ f\circ \varphi(x_1,\dots,x_n)=(x_1,\dots, x_r,0,\dots,0)\]
\end{theorem}
\begin{proof}
	Soit $(e'_1,\dots e_m')$ base de $\R^m$, $x_0\in U$ on note $A=\d f_{x_0}$, on a par hypothèse $\rg(A)=r$. On choisit alors pour base de $\R^n$ $(e_1,\dots, e_r,e_{r+1},\dots, e_n)$ tels que $A(e_i)=e_i'$ pour $i\in \llb 1,r\rrb$. Du théorème du rang, on aura : $\Ker(A)=\Vect(e_{r+1},\dots, e_n)$ et dans ces bases on aura:
	\[\d f_{x_0}(x_1,\dots,x_n)=(x_1,\dots,x_r,0,\dots,0)\]
	On considère alors la projection : $\pi:\R^m\to \R^r,\ (x_1,\dots,x_m)\longmapsto(x_1,\dots,x_r)$ on pose alors:
	\[w:U\subset \R^n\to \R^n,\ (x_1,\dots, x_n)\longmapsto(\pi(f(x_1,\dots,x_n)),x_{r+1},\dots,x_n)\]
	On a $\d w_{x_0}=I$ car $\pi\circ \d f_{x_0}$ est l'identité sur $(x_1,\dots, x_r)$ et $w|_{\R^n\backslash\R^r}=\id$. Par le théorème de l'inversion locale, il existe $\varphi:U'\to U'$ difféomorphisme local sur $U'$ tel que: $w\circ \varphi=\id_{\R^n}$. Donc on a ;
	\[x=(x_1,\dots,x_n)=(\pi f\varphi(x),\varphi_{r+1}(x),\dots,\varphi_n(x))\]
	Alors on a $\pi f\varphi(x)=(x_1,\dots,x_r)$ et $(\varphi_{r+1}(x),\dots,\varphi_n(x))(x_{r+1},\dots,x_n)$ et donc :
	\[f\circ \varphi(x)=(x_1,\dots,x_r,g(x))\]
	où $g:\R^n\to \R^{m-r},\ x\longmapsto g(x)=(g_{r+1}(x),\dots,g_m(x))$.
\end{proof}

\begin{definition}
	Une application $f:U\subset \R^n\to\R^m$ est une immersion (resp. submersion) en un point $x_0\in U$ si $\d f_{x_0}:\R^n\to \R^m$ est injective (resp. surjective). Si cette propriété est vrai pour tout $x_0\in U$ $f$ est une immersion (resp. submersion) sur $U$.
\end{definition}
\begin{proposition}
	Soit $f:U\subset \R^n\to\R^m$ une application de classe $C^1$. Alors les affirmations suivantes sont vraies:
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item Si $f$ est une immersion en $x_0\in U$ il existe des voisinages $U_{x_0}\subset U,\ V_{f(x_0)=y_0}\subset V$ et des difféomorphismes $\varphi:U_{x_0}\to U_{x_0}$ et $\psi:V_{y_0}\to V_{y_0}$ tels que:
		\[\forall x=(x_1,\dots,x_n)\in U_{x_0},\ \psi f\varphi(x)=(x_1,\dots,x_n,0,\dots,0)\]
		\item si $f$ est une submersion en $x_0$ alors avec les mêmes notations que dans i) on a :
		\[\psi f\varphi(x)=(x_1,\dots,x_m)\]
	\end{enumerate}
\end{proposition}
\begin{proof}
	\begin{enumerate}[label=(\roman*),leftmargin=*]
		\item On a $\d f_{x_0}$ est injective, donc la matrice de Jacobi possède un mineur $M_{x_0}$ de taille $n\times n$ non-nul. Puisque $f$ est de classe $C^1$ on a un voisinage $U_{x_0}\subset U$ tel que pour tout $x$ le mineur $M_x$ constitué des mêmes colonnes et lignes que $M_{x_0}$ est proche de $M_{x_0}$ et donc est aussi non nul. Donc on a pour tout $x\in U_{x_0}$ $f$ une immersion et donc $\rg(\d f_x)=n$. En appliquant le théorème du rang constant pour $U_{x_0}$ et $f(U_{x_0})$ on obtient le résultat.
		\item Idem, s'il existe un mineur non-nul de taille $m\times m$ au point $x_0$ il existe voisinage $U_{x_0}$ tel que $\forall x\in U_{x_0},\ M_x$ de taille $m\times m$ et $|M_x|\neq 0$.
	\end{enumerate}
\end{proof}

\section{Introduire notions de points et valeur critique pour application différentiable. Enoncé théorème de Morse-Sard. Donner example si on peut remplacer valeur par point critique. Preuves: théoreme de Brown (densité de valeurs critiques) et les lignes de niveaux de deux applications dérivable de $\R^2$ à $\R$}
\begin{definition}
	Soit $f:\R^n\to\R^m$ une application différentiable. Un point $x\in\R^n$ est dit critique si $\rg(\d f_x)<m$ on les notes $PC(f)$. Un point $y\in\R^m$ est dit valeur critique s'il existe $x\in PC(f)$ tel que $y=f(x)$ on les note $vc(f)$. Un point $z\in\R^n$ est régulier si $z\notin PC(f)$ et $w$ est une valeur régulière si $w\notin vc(f)$.
\end{definition}
\begin{theorem}{ de Morse-Sard}
	Soit $f:U\subset\R^n\to\R^m$ une application différentiable de classe $C^k$ où $k\geq\max(\tfrac{n}{m},1)$. Alors l'ensemble des valeurs critiques $vc(f)$ est de mesure de Lebesgue nulle:
	\[\lambda_{\R^m}(f(C)=\{y\in\R^m\mid y=f(x)\text{ et }\rg(\d f_x)<m\})=0\]
\end{theorem}
\begin{corollary}
	Dans les hypothèses du théorème. L'ensemble des valeurs régulières de $f$ : $vr(f)=\R^m\backslash vc(f)$ est dense dans $\R^m$.
\end{corollary}
\begin{proof}
	Par l'absurde, on suppose que $vr(f)$ non dense dans $\R^m$, il existe donc une boule ouverte $\B$ de $\R^m$ qui ne rencontre pas $vr(f)$ donc $\B\subset \R^m\backslash vr(f)=vc(f)$ or $\lambda_{\R^m}(\B)>0$ et par théorème de Morse-Sard $\lambda_{\R^m}(vc(f)$ est nulle, ce qui est absurde. D'où $vr(f)$ est dense.
\end{proof}

\section{Introduire sous variété de $\R^n$. Donner exemples. Introduire dimension d'une sous-variété et démontrer qu'elle ne dépend pas du point et reste constante pour une sous variété connexe. Démontrer image réciproque d'une valeur régulière d'une application de classe $C^1$ entre $\R^n$ et $\R^m$ est une sous variété de dim $n-m$, démonter que la sphère $S^n$ est une sous-variété de $\R^n$. Démontrer que le produit de deux sous-variétés est une sous-variété en précisant sa dimension. En déduire que le tore $\mathbb{T}^n$ est une sous-variété de dimension $n$ de $\R^{2n}$}
\begin{definition}
	Un sous-ensemble $M$ de $\R^n$ est une sous variété de dimension $m\ (0\leq m\leq n)$ si pour tout point $x\in M$ il existe deux ouvert $U$ et $V$ de $\R^n$ tels que $x\in U$ et $0\in V$ et un difféomorphisme 
	\[\varphi:U\to V\text{ tel que }\varphi(U\cap M)=V\cap (\R^m \times \{0\})\] 
\end{definition}
\section{Introduire espace tangent d'une sous-variété. Démontrer que la définition est équivalente à sa forme utilisant la differentielle de l'application de paramétrage. Espaces tangents des sous-variétés du type "image réciproque d'une valeur régulière" et du graphe d'une application de classe $C^1$. Déterminer le plan tangent de la sphère $S^n$.}

\section{Définir une application différentiable entre deux sous-variétés et démontrer la condition équivalente en utilisant les cartes. Pour une applications $f:M\tto N$ entre deux sous-variétés définir $\d f_x$, ainsi que les valeurs critiques et régulière. En utilisant le théorème de Morse-Sard dans les espaces euclidiens démontrer le théorème de Morse-Sard pour $f$ (en justifiant que la preuve ne dépend pas du choix de cartes). Énoncer et démontrer le théorème de Brown.}






\end{document}
