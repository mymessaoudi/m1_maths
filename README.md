# Cours M1 maths

## Semestre 1

### Analyse fonctionnelle

+ Cours [[pdf](semestre1/analyse/notes_cours.pdf)]
+ Fiches tds:
  + Espaces de fonctions [[fiche](semestre1/analyse/tds/td1_fiche.pdf)]
  + Théorèmes d'analyse fonctionnelle [[fiche](semestre1/analyse/tds/td2_fiche.pdf)] [[corrigé](semestre1/analyse/tds/td2_corrige.pdf)]
  + Espaces de Hilbert [[fiche](semestre1/analyse/tds/td3_fiche.pdf)] [[corrigé](semestre1/analyse/tds/td3_corrige.pdf)]
  + Espaces <img src ="https://render.githubusercontent.com/render/math?math=\mathrm{L}^p">, convolution, transformée de Fourier [[fiche](semestre1/analyse/tds/td4_fiche.pdf)] [[corrigé](semestre1/analyse/tds/td4_corrige.pdf)]
+ Devoir Non Surveillé [[sujet](semestre1/analyse/examens/dns_sujet.pdf)]
+ Partiel [[sujet](semestre1/analyse/examens/partiel_2020-2021.pdf)] [[corrigé](semestre1/analyse/examens/partiel_2020-2021_corrige.pdf)]
+ Examen [[sujet](semestre1/analyse/examens/exam_2020-2021.pdf)] [[corrigé](semestre1/analyse/examens/exam_2020-2021_corrige.pdf)]

### Groupe et géométrie

+ Cours
+ Fiches tds:
  + Compléments sur les groupes [[fiche](semestre1/groupe_geometrie/tds/td1_fiche.pdf)]
  + Représentations linéaires des groupes finis (1) [[fiche](semestre1/groupe_geometrie/tds/td2_fiche.pdf)]
  + Représentations linéaires des groupes finis (2) [[fiche](semestre1/groupe_geometrie/tds/td3_fiche.pdf)]
  + Représentations linéaires des groupes finis (3) [[fiche](semestre1/groupe_geometrie/tds/td4_fiche.pdf)]
  + Formes quadratiques et groupes orthogonaux en dimension finie (1) [[fiche](semestre1/groupe_geometrie/tds/td5_fiche.pdf)]
  + Formes quadratiques et groupes orthogonaux en dimension finie (2) [[fiche](semestre1/groupe_geometrie/tds/td6_fiche.pdf)]
  + Formes quadratiques et groupes orthogonaux en dimension finie (3) [[fiche](semestre1/groupe_geometrie/tds/td7_fiche.pdf)]
  + Formes quadratiques et groupes orthogonaux en dimension finie (4) [[fiche](semestre1/groupe_geometrie/tds/td8_fiche.pdf)]
  + Formes quadratiques et groupes orthogonaux en dimension finie (5) [[fiche](semestre1/groupe_geometrie/tds/td9_fiche.pdf)]
+ Partiel [[sujet](semestre1/groupe_geometrie/examens/partiel_sujet.pdf)] [[corrigé](semestre1/groupe_geometrie/examens/partiel_corrigé.pdf)]
+ Examen [[sujet](semestre1/groupe_geometrie/examens/examen_sujet.pdf)]

### Géométrie différentielle

+ Cours [[pdf](semestre1/geo_diff/qst_theorique.pdf)]
+ Fiches tds:
  + Géométrie différentielle des courbes dans <img src ="https://render.githubusercontent.com/render/math?math=\mathbb{R}^n"> [[fiche](semestre1/geo_diff/tds/fiche1_geom_courbes.pdf)]
  + Géométrie des surfaces dans <img src ="https://render.githubusercontent.com/render/math?math=\mathbb{R}^3"> [[fiche](semestre1/geo_diff/tds/fiche2_surfaces.pdf)]
  + Applications différentielles, immersions, submersions [[fiche](semestre1/geo_diff/tds/fiche3_SubImm.pdf)]
+ Devoir Non Surveillé [[sujet](semestre1/geo_diff/examens/dns_sujet.pdf)] [[corrigé](semestre1/geo_diff/examens/dns_corrigé.pdf)]
+ Partiel [[sujet](semestre1/geo_diff/examens/partiel_sujet.pdf)] [[corrigé](semestre1/geo_diff/examens/partiel_corrigé.pdf)]
+ Examen [[sujet](semestre1/geo_diff/examens/examen_sujet.pdf)] [[corrigé](semestre1/geo_diff/examens/examen_corrigé.pdf)]

## Semestre 2

### Analyse complexe

+ Cours [[pdf](semestre2/analyse_complexe/cours/cours.pdf)]
+ Fiches tds:
  + Fonctions holomorphes [[fiche](semestre2/analyse_complexe/tds/td_fiche1.pdf)]
  + Théorème des résidus et applications [[fiche](semestre2/analyse_complexe/tds/td_fiche2.pdf)]
  + Suites de fonctions holomorphes [[fiche](semestre2/analyse_complexe/tds/td_fiche3.pdf)]
  + Séries de fonctions méromorphes [[fiche](semestre2/analyse_complexe/tds/td_fiche4.pdf)]
  + Produits infinis [[fiche](semestre2/analyse_complexe/tds/td_fiche5.pdf)]
  + Prolongement analytique et logarithme complexe [[fiche](semestre2/analyse_complexe/tds/td_fiche6.pdf)]

### Équations aux dérivées partielles

+ Cours [[pdf](semestre2/edp/cours/cours.pdf)] :
  + 13 janvier 2021 [[pdf](semestre2/edp/cours/cours1.pdf)]
  + 20 janvier 2021 [[pdf](semestre2/edp/cours/cours2.pdf)]
  + 27 janvier 2021 [[pdf](semestre2/edp/cours/cours3.pdf)]
  + 03 février 2021 [[pdf](semestre2/edp/cours/cours4.pdf)]
+ Fiches tds:
  + Équations elliptiques [[fiche](semestre2/edp/tds/td_fiche1.pdf)]
  + Équation de la chaleur [[fiche](semestre2/edp/tds/td_fiche2.pdf)]
  + Analyse numérique matricielle pour les EDP [[fiche](semestre2/edp/tds/td_fiche3.pdf)]
+ Séance tds:
  + 20 janvier 2021 [[pdf](semestre2/edp/tds/td_seance1.pdf)]
  + 27 janvier 2021 [[pdf](semestre2/edp/tds/td_seance2.pdf)]
  + 03 février 2021 [[pdf](semestre2/edp/tds/td_seance3.pdf)]
+ Devoir Non Surveillé [[sujet](semestre2/edp/examens/dns_sujet.pdf)] [[corrige](semestre2/edp/examens/dns_corrige.pdf)]
+ Examen [[sujet](semestre2/edp/examens/examen_sujet.pdf)]

### Topologie Algébrique

+ Cours [[pdf](semestre2/topo_algébrique/cours/cours.pdf)] :
  + Chapitre 1: Espaces topologiques [[pdf](semestre2/topo_algébrique/cours/chap1/chap1.pdf)]
  + Chapitre 2: Homotopie et groupe fondamental [[pdf](semestre2/topo_algébrique/cours/chap2/chap2.pdf))]
  + Chapitre 3: Introduction à l'homologie [[pdf](semestre2/topo_algébrique/cours/chap3/chap3.pdf))]
  + Complement: Classification des surfaces et calcul du groupe fondamental [[pdf](semestre2/topo_algébrique/cours/complement.pdf)]
+ Fiches tds:
  + Topologie générale: notions basiques [[fiche](semestre2/topo_algébrique/tds/td_fiche1.pdf)]
  + Propriétés basiques des espaces [[fiche](semestre2/topo_algébrique/tds/td_fiche2.pdf)]
  + Espaces quotients et variétés [[fiche](semestre2/topo_algébrique/tds/td_fiche3.pdf)]
  + Homotopie [[fiche](semestre2/topo_algébrique/tds/td_fiche4.pdf)]
  + Groupe fondamental [[fiche](semestre2/topo_algébrique/tds/td_fiche5.pdf)]
  + Théorème de van Kampen [[fiche](semestre2/topo_algébrique/tds/td_fiche6.pdf)]
  + Homologie [[fiche](semestre2/topo_algébrique/tds/td_fiche7.pdf)]
+ Séance tds:
  + 21 janvier 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance1.pdf)]
  + 28 janvier 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance2.pdf)]
  + 04 février 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance3.pdf)]
  + 11 février 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance4.pdf)]
  + 16 février 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance5.pdf)]
  + 04 mars 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance6.pdf)]
  + 18 mars 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance7.pdf)]
  + 25 mars 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance8.pdf)]
  + 01 avril 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance9.pdf)]
  + 08 avril 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance10.pdf)]
  + 15 avril 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance11.pdf)]
  + 22 avril 2021 [[pdf](semestre2/topo_algébrique/tds/td_seance12.pdf)]
+ Devoir Non Surveillé [[sujet](semestre2/topo_algébrique/examens/dns_sujet.pdf)] [[corrige](semestre2/topo_algébrique/examens/dns_corrige.pdf)]
+ Examen [[sujet](semestre2/topo_algébrique/examens/examen_sujet.pdf)] [[corrige](semestre2/topo_algébrique/examens/examen_corrige.pdf)]

### Anglais

+ Cours:
  + Séance 1 [[pdf](semestre2/anglais/seance1.pdf)]
  + Séance 2 [[pdf](semestre2/anglais/seance2.pdf)]
  + Séance 3 [[pdf](semestre2/anglais/seance3.pdf)]
  + Séance 4 [[pdf](semestre2/anglais/seance4.pdf)]
  + Séance 5 [[pdf](semestre2/anglais/seance5.pdf)]
  + Séance 6 [[pdf](semestre2/anglais/seance6.pdf)]

### TER

+ Sujets TER [[pdf](semestre2/ter_sujets.pdf)]
